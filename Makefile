SHELL=/bin/bash -o pipefail

setup:
	@echo "------Installing testapps------"
	pip install -e testapps
	@echo "Setup complete"

test_all:	
	@echo "=====Running all tests====="
	pytest -v --cheetah="/home/bshaw/cheetah_build/cheetah/pipeline/cheetah_pipeline" --html=report.html --self-contained-html --capture=sys --log-file-level=INFO

test_all_retain:
	@echo "=====Running all tests====="
	pytest -v -m "not clean" --cheetah="/home/bshaw/cheetah_build/cheetah/pipeline/cheetah_pipeline" --html=report.html --self-contained-html --capture=sys --log-file-level=INFO

clean:
	@echo "=====Running clean scripts====="
	pytest -v -m "clean" --cheetah="/home/bshaw/cheetah_build/cheetah/pipeline/cheetah_pipeline" --html=report.html --self-contained-html --capture=sys --log-file-level=INFO

test_spsem:
	@echo "=====Running SPS emulator tests====="
	pytest -v -m "spsem" --cheetah="/home/bshaw/cheetah_build/cheetah/pipeline/cheetah_pipeline" --html=report.html --self-contained-html --capture=sys --log-file-level=INFO

test_sps:
	@echo "=====Running SPS pipeline tests====="
	pytest -v -m "sps" --cheetah="/home/bshaw/cheetah_build/cheetah/pipeline/cheetah_pipeline" --html=report.html --self-contained-html --capture=sys --log-file-level=INFO

test_system:
	@echo "=====Running system tests====="
	pytest -v -m "system" --cheetah="/home/bshaw/cheetah_build/cheetah/pipeline/cheetah_pipeline" --html=report.html --self-contained-html --capture=sys --log-file-level=INFO
