#!/usr/bin/env python

import os
from pytest import mark, fixture
import testapps as tp
import shutil

CONF_DIR = "configs"
SPS_EMUL_CONF = str(CONF_DIR) + "/mvp_emulator_config.xml"

@mark.spsem
class SpsemFailTests:

    @fixture(scope='class')
    def setup(self):
        spccl_dir = tp.Config(xmlfile=SPS_EMUL_CONF).spccldir()
        if os.path.isdir(spccl_dir):
            shutil.rmtree(spccl_dir)
        os.mkdir(spccl_dir)


    @mark.system
    def test_system_for_sps_emulator_default_bad_system(self):
        this_system = tp.System()
        minimum_cpus = 100
        minimum_memory = 3e12
        min_ngpus = 10
        ncpus = this_system.cpuinfo()[0]
        mem = this_system.meminfo()
        ngpus = this_system.gpu_count()
        assert ncpus <= minimum_cpus
        assert mem <= minimum_memory
        assert ngpus <= min_ngpus

    def test_sps_emulator_execution_wrong_pipeline_name(self, cheetah, setup):
        setup
        instance = tp.Pipeline(
            cheetah,
            "sigproc",
            "SinglePuls",
            SPS_EMUL_CONF,
            "debug"
            )
        instance.run()
        assert instance.err is not None
        assert instance.ec == 0


    def test_sps_emulator_execution_wrong_source_name(self, cheetah, setup):
        setup
        instance = tp.Pipeline(
            cheetah,
            "sigpro",
            "SinglePulse",
            SPS_EMUL_CONF,
            "debug"
            )
        instance.run()
        assert instance.err is not None
        assert instance.ec == 0


    def test_sps_emulator_execution_wrong_exec_name(self, setup):
        setup
        instance = tp.Pipeline(
            "/path/to/wrong/place",
            "sigproc",
            "SinglePulse",
            SPS_EMUL_CONF,
            "debug"
            )
        out, err, retcode = instance.run()
        assert err is None
        assert retcode is None
        assert out is None


    def test_sps_emulator_execution_wrong_config_name(self, cheetah, setup):
        setup
        instance = tp.Pipeline(
            cheetah,
            "sigproc",
            "SinglePulse",
            "nonexistent_file.xml",
            "debug"
            )
        instance.run()
        assert instance.err is not None
        assert instance.ec == 1

    @mark.clean
    def test_clean(self):
        spccldir = tp.Config(xmlfile=SPS_EMUL_CONF).spccldir()
        shutil.rmtree(spccldir)
        assert os.path.isdir(spccldir) == False
