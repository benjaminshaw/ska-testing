from pytest import fixture

def pytest_addoption(parser):
    parser.addoption("--cheetah",
                     action="store",
                     help="Path to cheetah executable"
                    )

@fixture(scope="session")
def cheetah(request):
    return request.config.getoption("--cheetah")
