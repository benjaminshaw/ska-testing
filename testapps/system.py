from typing import Tuple
import platform
import GPUtil
import psutil


class System():

    def __init__(self):
        self.uname = platform.uname()
        self.svmem = psutil.virtual_memory()
        self.swap = psutil.swap_memory()
        self.gpus = GPUtil.getGPUs()

    def system(self) -> str:
        """
        Returns the name of the OS

        Returns
        -------
        str
            The name of the OS
        """
        return self.uname.system

    def node(self) -> str:
        """
        Returns the hostname of the node

        Returns
        -------
        str
            The hostname
        """
        return self.uname.node

    def version(self) -> str:
        """
        Returns the version of the OS

        Returns
        -------
        str
            The OS version
        """
        return self.uname.release

    def arch(self) -> str:
        """
        Gets the architecture type

        Returns
        -------
        str
            The architecture type
        """
        return self.uname.machine

    def cpuinfo(self) -> Tuple[int, int]:
        """
        Gets the number of CPUS

        Returns
        -------
        Tuple
            The number of physical and total cpus
        """
        physical = psutil.cpu_count(logical=False)
        total = psutil.cpu_count(logical=True)
        return physical, total

    def meminfo(self) -> int:
        """
        Gets the total memory

        Returns
        -------
        int
            Memory size (bytes)
        """
        return self.svmem.total

    def swapinfo(self) -> int:
        """
        Gets the swap memory details

        Returns
        -------
        int
            Swap size (bytes)
        """
        return self.swap.total

    def gpu_count(self) -> int:
        """
        Gets the number of available GPUs

        Returns
        -------
        int
            The number of GPUs
        """
        ngpus = 0
        for gpu in self.gpus:
            ngpus += 1
        return ngpus
