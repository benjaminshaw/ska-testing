#!/usr/bin/env python

import logging
import os.path
from typing import Union
import re
import subprocess
import json
import sys

# pylint: disable=W1202,R0913

logging.basicConfig(format='[%(asctime)s][%(levelname)s][%(message)s]',
                    level=logging.INFO)


class Pipeline():

    _executables = {
        "cheetah_pipeline": "pipeline/cheetah_pipeline",
        "emulator": "emulator/cheetah_emulator"
        }

    _sources = [
        'sigproc',
        'udp_low',
        'udp']

    _pipelines = [
        'SinglePulse',
        'Empty',
        'Tdas',
        'RfiDetectionPipeline']

    def __init__(self, execu, src, pline, config, level='log'):
        self.execu = execu
        self.src = src
        self.pline = pline
        self.config = config
        self.level = level
        self.command = None

        self.formatted_out = None
        self.err = None
        self.ec = 0

        self.command = self._form_command()


    @staticmethod
    def _check_file(this_file: str) -> bool:
        """
        Checks the existence of a file

        Parameters
        ----------
        this_file : str
            File to check exists

        Returns
        -------
        bool
            True if file exists, False otherwise
        """
        if not os.path.isfile(this_file):
            logging.error("{} not found".format(this_file))
            return False
        logging.info("Found {}".format(this_file))
        return True

    def _form_command(self) -> Union[list, None]:
        """
        Takes inputs from constructor and forms command for
        cheetah execution.

        Parameters
        ----------
        None

        Returns
        -------
        command : array
            Command as numpy array
        """
        if not self._check_file(self.execu):
            return None
        this_config = "--config=" + self.config
        this_log = "--log-level=" + self.level
        command = [self.execu,
                   '-s', self.src,
                   '-p', self.pline,
                   this_config, this_log]
        logging.info("Command is: {}" .format(' '.join(command)))
        return command

    @staticmethod
    def _parse_stdout(logdata: str) -> str:
        """
        Parses STDOUT cheetah logs into a searchable format.
        Each line of log data is written as a dict with keys

        "type" (e.g., warn, log, debug)
        "tid" (the thread id number)
        "src" (the file which produced the message)
        "time" (the unix timestamp of the log)
        "msg" (the message itself)

        and returned as a json string.

        Parameters
        ----------
        logdata : str
            Cheetah output STDOUT log data

        Returns:
        -------
        str
            json string of cheetah log messages

        TODO: Carriage returns within a log message
              are not yet parsed correctly
        """
        fields = logdata.split("\n")
        data_out = []
        for line in fields:
            line_dict = {}
            if line.startswith('['):
                metadata = re.findall(r'\[(.+?)\]', line)
                line_dict["type"] = metadata[0]
                line_dict["tid"] = metadata[1].replace('tid=', '')
                line_dict["src"] = metadata[2]
                line_dict["time"] = metadata[3]
                line_dict["msg"] = line.split("]")[-1]
                data_out.append(line_dict)

        json_out = json.dumps(data_out, indent=4)
        return json_out

    def run(self):
        """
        Runs required cheetah pipeline with arguments as a child process

        Parameters
        ----------
        None

        Returns
        -------
        out : array
            Cheetah logs from STDOUT as a numpy array
        err : str
            Cheetah logs from STDERR
        exit_code : int
            Return code from cheetah execution
        """

        if not self.command:
            return None, None, None
        child = subprocess.Popen(
            self.command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
            )
        out, err = child.communicate()

        # Handle STDERR
        self.err = err.decode("utf-8")
        if len(self.err) == 0:
            self.err = None  
            logging.info("STDERR: {}".format(err))
        else:
            logging.warning("STDERR: {}".format(err))

        # Handle STDOUT
        out = out.decode("utf-8")
        self.formatted_out = self._parse_stdout(out)

        # Get exit code
        self.ec = child.returncode
        logging.info("Return code is: {}".format(self.ec))

        #return formatted_out, err, exit_code

    def print_pars(self) -> None:
        """
        Prints list of executables, pipelines and data sources to console.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        print("\nAvailable executables: <exec> <path>\n{}\n".format(Pipeline._executables))
        print("Available pipelines: \n{}\n".format(Pipeline._pipelines))
        print("Available sources: \n{}\n".format(Pipeline._sources))


class LogParse():

    def __init__(self, logs=None):
        self.logs = self._check_logs(logs)

    @staticmethod
    def _check_logs(logs: str) -> Union[str, bool]:
        """
        Checks and loads cheetah logs and ensures they are
        valid JSON

        Parameters
        ----------
        logs : str
            JSON string of cheetah logs

        Returns
        -------
        str, bool
            JSON string object if logs are valid, else False
        """
        try:
            this_logs = json.loads(logs)
            logging.info("Cheetah logs valid JSON")
            return this_logs
        except ValueError:
            logging.error("Cheetah logs not parsed")
            return False
        return False

    def search(self, item: str) -> bool:
        """
        Searches logs for string.

        Parameters
        ----------
        item : str
            Search string

        Returns
        -------
        bool
            True if string was found in log messages, else False
        """
        if not self.logs:
            logging.error("Logs invalid - not searching")
            return False
        logging.info("Searching logs for message: '{}'".format(item))
        messages = [msg.get("msg") for msg in self.logs]
        if item in messages:
            logging.info("String '{}' found".format(item))
            return True
        logging.info("String '{}' not found".format(item))
        return False

    def errors(self) -> Union[bool, str]:
        """
        Searches logs for messages with the "error" type

        Returns
        -------
        bool/str
            False if no errors are found,
            True if logs can'tbe parsed,
            else json string of log entries.
        """
        if not self.logs:
            logging.error("Logs invalid - not searching")
            return True
        errors_list = []
        logging.info("Searching for messages of type 'error'")
        for entry in self.logs:
            if entry["type"] == "error":
                errors_list.append(entry)
        if len(errors_list) == 0:
            logging.info("No log messages of type 'error'")
            return False
        json_out = json.dumps(errors_list, indent=4)
        return json_out

class Spccl():

    def __init__(self, path: str):
        self.path = path
        self.spccl = self._isfile(path)

    @staticmethod
    def _isfile(path: str) -> Union[str, bool]:
        """
        Checks for spccl file in directory

        Parameters
        ----------
        str : path
            path to directory in which spccls are written

        Returns
        -------
        str/bool
           Full path to spccl file, else False
        
        TODO: Assumes only one spccl in dir.
        """
        for this_file in os.listdir(path):
            if this_file.endswith(".spccl"):
                fullpath = os.path.join(path, this_file)
                return fullpath
        return False

    def candlist(self) -> Union[list, None]:
        """
        Reads spccl file and returns to user as array of arrays

        Returns
        -------
        list/None
            2D array of candidate parameters if file is found, else None
        """
        if not self.spccl:
            logging.info("Candidate list not found")
            return None
        this_candlist = []
        with open(self.spccl) as this_spccl:
            for line in this_spccl.readlines()[1:]:
               fields = line.split()
               this_candlist.append(fields)
        return this_candlist
