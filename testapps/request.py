#!/usr/bin/env python

import argparse
import logging
import os.path
import requests

logging.basicConfig(format='[%(asctime)s][%(levelname)s][%(message)s]',
                    level=logging.INFO)


class Vector():
    def __init__(self, period, width, dm, acc, snr, flux):
        self.period = period
        self.width = width
        self.dm = dm
        self.acc = acc
        self.snr = snr
        self.flux = flux

        self.cache_dir = os.path.expanduser("~") \
            + "/.cache/SKA/test_vectors"

        self.cache_dir = "./vectors/"

        logging.info("Setting cache directory: {}".format(self.cache_dir))

        self.filename = self._filename_gen()

    def _filename_gen(self) -> str:
        this_filename = "Default_1_" \
                        + str(self.period) + "_" \
                        + str(self.width) + "_" \
                        + str(self.dm) + "_" \
                        + str(self.acc) + "_Gaussian_" \
                        + str(self.snr) + "_" \
                        + str(self.flux) + ".fil"

        logging.info("Searching for test vector: {}".format(this_filename))
        return this_filename

    def _check_cache(self, filename: str) -> str:

        path = self.cache_dir + "/" + filename

        if not os.path.isdir(self.cache_dir):
            logging.warn("Creating {}".format(self.cache_dir))
            os.mkdir(self.cache_dir)
            return None
        else:
            if os.path.isfile(path):
                logging.info("{} in local cache".format(path))
                return path
            else:
                logging.info("{} not found in local cache".format(path))
                return None
        return None

    def _download(self, filename: str) -> str:
        logging.info("Pulling {} from remote server".format(filename))
        url = 'http://google.com/favicon.ico'
        r = requests.get(url, allow_redirects=True)
        path = self.cache_dir + "/" + filename
        open(path, 'wb').write(r.content)
        return path

    def pull(self) -> str:
        this_path = self._check_cache(self.filename)
        if this_path:
            return this_path
        else:
            this_path = self._download(self.filename)
            if this_path:
                return this_path
            else:
                logging.error("{} not found".format(self.filename))


def main():

    parser = argparse.ArgumentParser(description='Query/download test vectors')
    parser.add_argument('-p', '--period',
                        help='Pulsar period (s)', required=True)
    parser.add_argument('-w', '--width',
                        help='Pulse width (turns)', required=True)
    parser.add_argument('-d', '--dm',
                        help='Dispersion measure', required=True)
    parser.add_argument('-a', '--acc',
                        help='Acceleration', required=True)
    parser.add_argument('-s', '--snr',
                        help='Folded S/N', required=True)
    parser.add_argument('-f', '--flux',
                        help='Flux density (Jy)', required=True)
    args = parser.parse_args()

    vector = Vector(args.period,
                    args.width,
                    args.dm,
                    args.acc,
                    args.snr,
                    args.flux)
    vector.pull()


if __name__ == '__main__':
    main()
