#!/usr/bin/env python

import logging
import os
from lxml import etree
from bs4 import BeautifulSoup

logging.basicConfig(format='[%(asctime)s][%(levelname)s][%(message)s]',
                    level=logging.INFO)


class Config():
    def __init__(self, xmlfile=None, xmldir=None):
        self.xmlfile = xmlfile
        self.xmldir = xmldir

    def valfile(self) -> bool:
        """
        Checks that an input file is valid xml

        Parameters
        ----------
        None

        Returns
        -------
        bool
            True is file is valid xml, else false
        """
        if not self.xmlfile:
            logging.warning("File not specified")
            return False
        logging.info("Loading {}".format(self.xmlfile))
        this_xml = open(self.xmlfile)
        contents = str.encode(this_xml.read())
        try:
            root = etree.fromstring(contents)
            logging.info("{} is valid".format(self.xmlfile))
            return True
        except etree.XMLSyntaxError:
            return False
        return False

    def valdir(self) -> bool:
        """
        Checks all files in a directory are valid xml

        Parameters
        ----------
        None

        Returns
        -------
        bool
            True is file is valid xml, else false
        """
        if not self.xmldir:
            logging.warning("Directory not specified")
            return False
        logging.info("Loading {}".format(self.xmldir))
        filelist = self._getfiles(self.xmldir)
        if len(filelist) == 0:
            logging.error("No xml files in {}".format(self.xmldir))
            return False
        logging.info("Found {} files".format(len(filelist)))
        for this_file in filelist:
            this_xml = open(this_file)
            contents = str.encode(this_xml.read())
            try:
                root = etree.fromstring(contents)
                logging.info("{} is valid".format(this_file))
            except etree.XMLSyntaxError:
                logging.error("{} not valid".format(this_file))
                return False
        return True

    @staticmethod
    def _getfiles(directory: str) -> list:
        """
        Provides list of files in a directory

        Parameters
        ----------
        directory : str
            Directory whose files are required

        Returns
        -------
        list
            contents of directory as numpy list
        """
        files = []
        for xfile in os.listdir(directory):
            if xfile.endswith(".xml"):
                files.append(os.path.join(directory, xfile))
        return files

    def update_xml(self) -> None:
        """
        TODO
        """
        if not self.xmlfile:
            logging.error("No xml file to edit")
        tree = etree.parse(self.xmlfile)


    def spccldir(self) -> str:
        """
        Provides the directory of the .spccl files

        Parameters
        ----------
        None

        Returns
        -------
        str
            directory as string
        """
        if not self.xmlfile:
            logging.warning("File not specified")
            return False
        logging.info("FileLocation loading {}".format(self.xmlfile))
        this_xml = open(self.xmlfile)

        soup = BeautifulSoup(this_xml, "lxml-xml")
        spccldir = soup.spccl_files.find('dir')
        onlydir = spccldir.get_text()
        logging.info(".spccl files are written to {}".format(onlydir))

        return onlydir

    def vectordir(self) -> str:
        if not self.xmlfile:
            logging.warning("File not specified")
            return False
        logging.info("FileLocation loading {}".format(self.xmlfile))
        this_xml = open(self.xmlfile)
        soup = BeautifulSoup(this_xml, "lxml-xml")
        vector = soup.sigproc.find('file')
        this_vector = vector.get_text()
        return this_vector
