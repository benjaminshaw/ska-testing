#!/usr/bin/env python

import argparse
import numpy as np
import os


class VHeader():

    _inttypes = ['machine_id',
                 'telescope_id',
                 'data_type',
                 'nchans',
                 'nbits',
                 'nifs',
                 'scan_number',
                 'barycentric',
                 'pulsarcentric',
                 'nbeams',
                 'ibeam']

    _strtypes = ['source_name',
                 'rawdatafile']

    _dbltypes = ['tstart',
                 'tsamp',
                 'fch1',
                 'foff',
                 'refdm',
                 'az_start',
                 'za_start',
                 'src_raj',
                 'src_dej']

    _chrtypes = ['signed']

    def __init__(self, path):
        self.path = path
        self.header_pars = self._parse(self.path)

    @staticmethod
    def _read_string(infile: str) -> str:
        nchar = np.fromfile(infile, dtype=np.int32, count=1)[0]
        if nchar < 1 or nchar > 80:
            raise Exception("Cannot parse filterbank header (Nchar was {} when reading string).".format(nchar))
        byte_data = infile.read(nchar)
        string_data = byte_data.decode("UTF-8")
        return string_data

    @staticmethod
    def _get_size(filename: str) -> int:
        st = os.stat(filename)
        return st.st_size

    @staticmethod
    def _parse(path: str) -> dict:
        header = {}
        fil = open(path, 'rb')
        key = VHeader._read_string(fil)
        bytes_read = len(key) + 4

        if key == "HEADER_START":
            key = VHeader._read_string(fil)
            bytes_read = len(key) + 4

            while key != "HEADER_END":
                if key in VHeader._strtypes:
                    header[key] = VHeader._read_string(fil)
                    bytes_read += len(header[key]) + 4
                elif key in VHeader._inttypes:
                    header[key] = np.fromfile(fil, dtype=np.int32, count=1)[0]
                    bytes_read += 4
                elif key in VHeader._dbltypes:
                    header[key] = np.fromfile(fil, dtype=np.float64,
                                              count=1)[0]
                    bytes_read += 8
                elif key in VHeader._chrtypes:
                    header[key] = np.fromfile(fil, dtype=np.int8, count=1)[0]
                    bytes_read += 1
                else:
                    raise Exception("Cannot parse filterbank header, key '{}' not understood".format(key))

                key = VHeader._read_string(fil)
                bytes_read += len(key) + 4

        header["header_size"] = fil.tell()
        return header

    def allpars(self) -> dict:
        return self.header_pars

    def print_allpars(self) -> None:
        print(self.header_pars)

    def machine_id(self) -> int:
        return self.header_pars["machine_id"]

    def tel(self) -> str:
        return self.header_pars["telescope_id"]

    def fch1(self) -> float:
        return self.header_pars["fch1"]

    def chbw(self) -> float:
        return self.header_pars["foff"]

    def nchans(self) -> int:
        return self.header_pars["nchans"]

    def source_name(self) -> str:
        return self.header_pars["source_name"]

    def ra(self) -> float:
        return self.header_pars["src_raj"]

    def dec(self) -> float:
        return self.header_pars["src_dej"]

    def nbits(self) -> float:
        return self.header_pars["nbits"]

    def start_time(self) -> float:
        return self.header_pars["tstart"]

    def tsamp(self) -> float:
        return self.header_pars["tsamp"]

    def header_size(self) -> int:
        return self.header_pars["header_size"]

    def duration(self) -> float:
        file_size = VHeader._get_size(self.path)
        duration = ((file_size - self.header_size()) /
                    self.nchans()) * self.tsamp()
        return duration

    def data_size(self) -> int:
        file_size = VHeader._get_size(self.path)
        data_size = file_size - self.header_size()
        return data_size


def main():

    parser = argparse.ArgumentParser(description='XML validator')
    parser.add_argument('-f', '--filename',
                        help='Filterbank file', required=True)
    args = parser.parse_args()

    filterbank = VHeader(args.filename)
    filterbank.print_allpars()


if __name__ == '__main__':
    main()
