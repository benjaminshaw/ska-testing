# PSS-Pipelinei SuperBuild. Integrating all parts for the SKA Pulsar Search Pipeline


## Obtaining the source code
- clone the git repository (https://gitlab.com/ska-telescope/pss-pipeline)
```
git clone --recursive git@gitlab.com:ska-telescope/pss-pipeline.git
git submodule update
```
If you forgot the --recursive flag, you will have to initialise the cloned area:
```
`git submodule init; git submodule update`
```
### Requried System Dependencies
To build the following will need to be installed on your system:
- A C++ compiler (supporting C++ 2011)
- cmake

Please see ![dependencies.txt](./dependencies.txt) for a definitive list of
other dependencies required.

### Other Dependencies
The thirdparty directory contains other dependencies required. These are managed
with git submodules and git subtrees. To update any of these please see the ![README](thirdpart/README)
in the thirdparty directory.

## Installation
This is a cmake project. Please see CMakeLists.txt file for build instructions

## Contributing
Please see doc/developers_guide
