# use the official gcc image, based on debian
# can use verions as well, like gcc:5.2
# see https://hub.docker.com/_/gcc/
#
# This base image is based on debian:buster-slim and contains:
#  * gcc 8.3.0
#  * clang 7.0.1
#  * cmake 3.13.4
#  * and more
#
# For details see https://github.com/ska-telescope/cpp_build_base
#

variables:
  DOCKER_DEBIAN_BUILD_IMAGE: nexus.engageska-portugal.pt/ska-docker/cpp_build_base

.common: {tags: [engageska, docker]}

stages:
  - build
  - linting
  - test
  - pages

before_script:
  - /usr/bin/apt-get update
  - ./tools/install_dependencies --distro Ubuntu

build_debian_debug:
  extends: .common
  stage: build
  image: ${DOCKER_DEBIAN_BUILD_IMAGE}
  script:
    - mkdir build_debug
    - cd build_debug
    - cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="-coverage" -DCMAKE_EXE_LINKER_FLAGS="-coverage"
    - make
  artifacts:
    paths:
      - build_debug

build_debian_release:
  extends: .common
  stage: build
  image: ${DOCKER_DEBIAN_BUILD_IMAGE}
  script:
    - mkdir build
    - cd build
    - cmake .. -DCMAKE_BUILD_TYPE=Release
    - make
  artifacts:
    paths:
      - build

build_export_compile_commands:
  extends: .common
  stage: build
  image: ${DOCKER_DEBIAN_BUILD_IMAGE}
  script:
    - rm -rf build && mkdir build
    - cd build
    - cmake --version
    - cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_CXX_COMPILER=clang++
  artifacts:
    paths:
      - build

lint_clang_tidy:
  extends: .common
  stage: linting
  image: ${DOCKER_DEBIAN_BUILD_IMAGE}
  needs:
    - job: build_export_compile_commands
  script:
    - cd build
    - run-clang-tidy -checks='cppcoreguidelines-*,performance-*,readibility-*,modernize-*,misc-*,clang-analyzer-*,google-*'

lint_iwyu:
  extends: .common
  stage: linting
  image: ${DOCKER_DEBIAN_BUILD_IMAGE}
  needs:
    - job: build_export_compile_commands
  script:
    - cd build
    - iwyu_tool -p .

lint_cppcheck:
  extends: .common
  stage: linting
  needs:
    - job: build_export_compile_commands
  script:
    - cd build
    - cppcheck --error-exitcode=1 --project=compile_commands.json -q --std=c++11 -i $PWD/../external -i $PWD/../src/ska/pss-pipeline/test

test_as_subproject:
  extends: .common
  stage: test
  script:
    - cp -r examples/pss-pipeline-user ..
    - cd ../pss-pipeline-user
    - sed -i 's/find_package(PssPipeline)/add_subdirectory(..\/cpp-template pss-pipeline EXCLUDE_FROM_ALL)/' CMakeLists.txt
    - if [ -d build ]; then rm -rf build; fi
    - mkdir build
    - cd build
    - cmake ..
    - make all

# A job that runs the tests under valgrid
# It might take a while, so not always run by default
test_memcheck:
  extends: .common
  stage: test
  before_script:
    - apt update && apt install -y valgrind
  script:
    - cd build
    - ctest -T memcheck
  only:
    - tags
    - schedules

pages:
  extends: .common
  stage: pages
  before_script:
    - apt update && apt install -y curl
  script:
    - mkdir -p .public/build/reports
    - cd .public
    - gcovr -r ../ -e '.*/external/.*' -e '.*/CompilerIdCXX/.*' -e '.*/tests/.*' --html --html-details -o index.html
    - gcovr -r ../ -e '.*/external/.*' -e '.*/CompilerIdCXX/.*' -e '.*/tests/.*' --xml -o build/reports/code-coverage.xml
    - cp ../build/ctest.xml build/reports/unit-tests.xml
    # Create and upload GitLab badges
    - python ../.produce-ci-metrics.py build/reports > ci-metrics.json
    - curl -s https://gitlab.com/ska-telescope/ci-metrics-utilities/raw/master/scripts/ci-badges-func.sh | sh
    - cd ..
    - mv .public public
  artifacts:
    paths:
      - public
