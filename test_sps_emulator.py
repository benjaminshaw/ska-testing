#!/usr/bin/env python

import os
import shutil
from pytest import mark, fixture
import testapps as tp

CONF_DIR = "configs"
SPS_EMUL_CONF = str(CONF_DIR) + "/mvp_emulator_config.xml"

@mark.spsem
class SpsemTests:


    @fixture(scope='class')
    def cheetah_instance(self, cheetah):
        this_instance = tp.Pipeline(cheetah,
                                    "sigproc",
                                    "SinglePulse",
                                    SPS_EMUL_CONF,
                                    "debug")
        return this_instance

    @fixture(scope='class')
    def setup(self):
        spccl_dir = tp.Config(xmlfile=SPS_EMUL_CONF).spccldir()
        if os.path.isdir(spccl_dir):
            shutil.rmtree(spccl_dir)
        os.mkdir(spccl_dir)

    @mark.system
    def test_system_for_sps_emulator_default(self):
        this_system = tp.System()
        minimum_cpus = 4
        minimum_memory = 3e9
        min_ngpus = 0
        ncpus = this_system.cpuinfo()[0]
        mem = this_system.meminfo()
        ngpus = this_system.gpu_count()
        assert ncpus >= minimum_cpus
        assert mem >= minimum_memory
        assert ngpus >= min_ngpus

    @mark.xfail(reason="Vector pull isn't yet implemented")
    def test_vector_pull_and_cache(self):
        vector = tp.Vector(0.5, 0.05, 100, 0.0, 100, 0.0005)
        this_vector = vector.pull()
        path = "/home/bshaw/.cache/SKA/test_vectors/Default_1_0.5_0.05_100_0.0_Gaussian_100_0.0005.fil"
        assert this_vector == path

    def test_sps_emulator_config_valid(self):
        result = tp.Config(xmlfile=SPS_EMUL_CONF).valfile()
        expected = True
        assert result == expected

    def test_sps_emulator_execution(self, cheetah_instance, setup):
        cheetah_instance.run()
        setup
        vector = tp.Config(xmlfile=SPS_EMUL_CONF).vectordir()
        fil_duration = int(tp.VHeader(vector).duration())
        expected_string = "Total number of candidates: " + str(fil_duration)
        logs = tp.LogParse(cheetah_instance.formatted_out)
        assert logs.search(expected_string)
        assert not logs.errors()
        assert cheetah_instance.err is None
        assert cheetah_instance.ec == 0

    def test_sps_emulator_spccl_in_bounds(self):
        spccl_dir = tp.Config(xmlfile=SPS_EMUL_CONF).spccldir()
        vector = tp.Config(xmlfile=SPS_EMUL_CONF).vectordir()
        cands = tp.Spccl(spccl_dir).candlist()
        assert cands is not None
        start_time = tp.VHeader(vector).start_time()
        dm_min, dm_max = 0, 10
        width_min, width_max = 1, 10
        sn_min, sn_max = 0, 100
        for cand in cands:
            assert float(cand[0]) >= start_time
            assert float(cand[1]) >= dm_min and float(cand[1]) <= dm_max
            assert float(cand[2]) >= width_min and float(cand[2]) <= width_max
            assert float(cand[3]) >= sn_min and float(cand[3]) <= sn_max

    @mark.clean
    def test_clean(self):
        spccldir = tp.Config(xmlfile=SPS_EMUL_CONF).spccldir()
        shutil.rmtree(spccldir)
        assert os.path.isdir(spccldir) == False
